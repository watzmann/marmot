package configuration

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/watzmann/marmot/remote"
)

func dummyserver(yml string) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, yml)
	}))
}

const (
	project1 = `
merge_request:
  weight:
    master: 1
    developer: 2
`
	wrongproject = `
merge_request:
  weights:
    master: 1
    developer: 2
`
)

func TestNewProject(t *testing.T) {
	type args struct {
		u    string
		resp string
		opts []remote.RemoteOpt
	}
	tests := []struct {
		name    string
		args    args
		want    *Project
		wantErr bool
	}{
		{name: "Illegal URL", args: args{u: "myscheme://illegalurl", resp: ""}, want: nil, wantErr: true},
		{name: "Legal Values", args: args{resp: project1}, want: &Project{
			MergeRequest: MergeRequest{
				Weight: Weight{
					Master:    1,
					Developer: 2,
				}}},
			wantErr: false},
		{name: "Illegal yaml", args: args{resp: wrongproject}, want: nil, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := dummyserver(tt.args.resp)
			if tt.args.u == "" {
				tt.args.u = srv.URL
			}
			got, err := NewProject(tt.args.u, tt.args.opts...)
			srv.Close()

			if (err != nil) != tt.wantErr {
				t.Errorf("NewProject() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewProject() = %v, want %v", got, tt.want)
			}
		})
	}
}
