package configuration

import (
	"fmt"
	"io/ioutil"
	"time"

	"gitlab.com/watzmann/marmot/remote"
	"gopkg.in/yaml.v2"
)

// A MergeRequest describes the settings for an MR event.
type MergeRequest struct {
	Id                 int
	AuthorId           int
	Assignee           int
	Weight             `yaml:"weight"`
	AuthorCanVote      bool `yaml:"author_can_vote"`
	RequiredWeight     int  `yaml:"required_weight"`
	MergeOnAgreement   bool `yaml:"merge_on_agreement"`
	RemoveSourceBranch bool `yaml:"remove_source_branch"`
	WhenPipelineFails  bool `yaml:"when_pipeline_fails"`
}

// Weight defines the weights of a lgtm of the given role
type Weight struct {
	Owner     int `yaml:"owner"`
	Master    int `yaml:"master"`
	Developer int `yaml:"developer"`
	Reporter  int `yaml:"reporter"`
	Guest     int `yaml:"guest"`
}

// Project stores the settings of a specific project. This configuration
// describes marmots beahaviour for a given project.
type Project struct {
	Id           int
	LastCommit   time.Time
	MergeRequest `yaml:"merge_request"`
}

// NewProject creates a new project from the given URL or
// returns an error.
func NewProject(u string, opts ...remote.RemoteOpt) (*Project, error) {
	resp, err := remote.New(opts...).Client().Get(u)
	if err != nil {
		return nil, fmt.Errorf("cannot get project from URL %q: %v", u, err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("cannot fetch data from body: %v", err)
	}
	if resp.StatusCode/100 != 2 {
		return nil, fmt.Errorf("illegal response (%s): %s", resp.Status, string(data))
	}
	return FromData(data)
}

func FromData(content []byte) (*Project, error) {
	var res Project
	err := yaml.UnmarshalStrict(content, &res)
	if err != nil {
		return nil, fmt.Errorf("cannot unmarshal the loaded file to a project: %v", err)
	}
	return &res, nil

}
