FROM ulrichschreiner/cacerts

COPY bin/marmot /marmot

CMD ["serve", "0.0.0.0:8080"]
ENTRYPOINT ["/marmot"]