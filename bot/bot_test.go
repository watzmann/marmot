package bot

import "testing"

func TestWithSecretToken(t *testing.T) {
	tests := []struct {
		name  string
		token string
		want  string
	}{
		{"No token given", "", ""},
		{"Secret token given", "a token", "a token"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := New("", "", WithSecretToken(tt.token))
			if m.secretToken != tt.want {
				t.Errorf("Marmot.secretToken = %v, want %v", m.secretToken, tt.want)
			}
		})
	}

}
