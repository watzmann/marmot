package bot

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/inconshreveable/log15"
	"gitlab.com/watzmann/marmot/configuration"
	"gitlab.com/watzmann/marmot/gitlab"

	gl "github.com/xanzy/go-gitlab"
)

const (
	eventHeader = "X-Gitlab-Event"
	tokenHeader = "X-Gitlab-Token"

	configurationFile = ".marmot.yml"

	cmdLgtm   = "/lgtm"
	cmdRating = "/rating"
)

var defaultConfig = configuration.Project{
	MergeRequest: configuration.MergeRequest{
		Weight: configuration.Weight{
			Owner:     1,
			Master:    1,
			Developer: 1,
			Reporter:  0,
			Guest:     0,
		},
		AuthorCanVote:      false,
		RequiredWeight:     2,
		RemoveSourceBranch: false,
		WhenPipelineFails:  false,
		MergeOnAgreement:   false,
	},
}

type MarmotOpt func(*Marmot)

// WithSecretToken tells marmot to check every incoming request
// to have a header with the given token
func WithSecretToken(token string) MarmotOpt {
	return func(m *Marmot) {
		m.secretToken = token
	}
}

// WithPrivateToken tells marmot its own identiy.
func WithPrivateToken(token string) MarmotOpt {
	return func(m *Marmot) {
		m.privateToken = token
	}
}

func WithGitlabURL(u string) MarmotOpt {
	return func(m *Marmot) {
		m.gitlabURL = u
	}
}

func Insecure(b bool) MarmotOpt {
	return func(m *Marmot) {
		m.insecureSkipVerify = b
	}
}

type Marmot struct {
	*http.ServeMux
	revision           string
	builddate          string
	gitlabURL          string
	privateToken       string
	secretToken        string
	insecureSkipVerify bool
	GitlabClient       func() (*gitlab.Client, error)
}

func New(rev, builddate string, opts ...MarmotOpt) *Marmot {
	marmot := &Marmot{
		revision:  rev,
		builddate: builddate,
		ServeMux:  http.NewServeMux(),
	}
	marmot.HandleFunc("/", marmot.index)
	for _, o := range opts {
		o(marmot)
	}
	marmot.GitlabClient = marmot.glClient
	return marmot
}

func isError(w http.ResponseWriter, msg string, err error) bool {
	if err != nil {
		log15.Error(msg, "error", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return true
	}
	return false
}

func (m *Marmot) index(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if isError(w, "cannot read body", err) {
		return
	}
	evt := r.Header.Get(eventHeader)
	tok := r.Header.Get(tokenHeader)
	if m.secretToken != "" {
		if tok != m.secretToken {
			log15.Error("wrong secret token provided", "remote", tok)
			http.Error(w, "wrong secret token", http.StatusForbidden)
			return
		}
	}
	info := struct {
		Rev   string `json:"rev"`
		Build string `json:"build"`
	}{
		Rev:   m.revision,
		Build: m.builddate,
	}
	json.NewEncoder(w).Encode(info)

	go func() {
		cl, err := m.GitlabClient()
		if err != nil {
			log15.Error("cannot connect to gitlab", "error", err)
			http.Error(w, "cannot connect to gitlab", http.StatusInternalServerError)
			return
		}
		me, err := cl.GetMe()
		if err != nil {
			log15.Error("cannot query myself", "error", err)
			http.Error(w, "cannot query myself", http.StatusInternalServerError)
			return
		}
		log15.Info("gitlab event", "event", evt)
		switch evt {
		case gitlab.EventMergeRequest:
			mr, err := gitlab.ParseMR(data)
			if isError(w, "cannot unmarshal MR", err) {
				return
			}
			if mr.ObjectAttributes.State != "opened" {
				log15.Info("mr not opened, ignore", "mr", mr)
				return
			}
			cfg, members, err := m.loadConfig(
				cl,
				mr.ObjectAttributes.TargetProjectID,
				mr.ObjectAttributes.IID,
				mr.ObjectAttributes.LastCommit.Timestamp,
				mr.ObjectAttributes.AuthorID,
				mr.ObjectAttributes.AssigneeID,
				mr.ObjectAttributes.TargetBranch)
			if isError(w, "cannot load configuration", err) {
				return
			}
			if isError(w, "MR note update", m.handleMRUpdated(cl, cfg, members, "")) {
				return
			}
		case gitlab.EventNote:
			note, err := gitlab.ParseMRNote(data)
			if isError(w, "unmarshal MR Note", err) {
				return
			}
			if note.MergeRequest.State != "opened" {
				log15.Info("mr not opened, ignore", "mr", note.MergeRequest)
				return
			}
			if note.ObjectAttributes.AuthorID == me.ID {
				log15.Info("note created by myself, ignoring")
				return
			}
			cfg, members, err := m.loadConfig(cl, note.ProjectID, note.MergeRequest.IID, note.MergeRequest.LastCommit.Timestamp, note.MergeRequest.AuthorID, note.MergeRequest.AssigneeID, note.MergeRequest.TargetBranch)
			if isError(w, "load configuration", err) {
				return
			}
			if isError(w, "MR note update", m.handleMRUpdated(cl, cfg, members, note.ObjectAttributes.Note)) {
				return
			}
		}
	}()
}

func (m *Marmot) loadConfig(cl *gitlab.Client, pid, mid int, lastcommit *time.Time, authorid, assignee int, ref string) (*configuration.Project, gitlab.ProjectMemberMap, error) {
	config := defaultConfig
	cfg := &config
	dat, err := cl.GetFile(pid, configurationFile, ref)
	if err == nil {
		cfg, err = configuration.FromData([]byte(dat))
		if err != nil {
			return nil, nil, err
		}
	}
	cfg.Id = pid
	cfg.MergeRequest.Id = mid
	cfg.MergeRequest.AuthorId = authorid
	cfg.MergeRequest.Assignee = assignee
	if lastcommit == nil {
		lastcommit = &time.Time{}
	}
	cfg.LastCommit = *lastcommit
	members, err := cl.GetProjectMembers(pid)
	if err != nil {
		return nil, nil, fmt.Errorf("cannot fetch members of project: %v", err)
	}

	return cfg, members, nil
}

func (m *Marmot) glClient() (*gitlab.Client, error) {
	cl, err := gitlab.New(m.gitlabURL, m.privateToken, m.insecureSkipVerify)
	if err != nil {
		return nil, err
	}
	_, err = cl.GetMe()
	if err != nil {
		return nil, err
	}
	return cl, nil
}

func (m *Marmot) handleMRUpdated(cl *gitlab.Client, cfg *configuration.Project, members gitlab.ProjectMemberMap, note string) error {
	h := mrNote(cl, cfg)
	// cl must be initizalized here, so "me" is initialized
	usr, _ := cl.GetMe()
	return h.NotesUpdated(members, usr.ID, note)
}

type mrNoteHandler struct {
	cl      *gitlab.Client
	project *configuration.Project
}

func mrNote(c *gitlab.Client, p *configuration.Project) *mrNoteHandler {

	return &mrNoteHandler{
		cl:      c,
		project: p,
	}
}

func (nh *mrNoteHandler) countLGTMs(members gitlab.ProjectMemberMap, notes []*gl.Note) (int, []string) {
	lgtm := 0
	votedusers := make(map[int]bool)
	if !nh.project.MergeRequest.AuthorCanVote {
		// disable the author of the MR to vote!
		votedusers[nh.project.MergeRequest.AuthorId] = true
	}
	var lgtms []string
	for _, n := range notes {
		if n.CreatedAt.After(nh.project.LastCommit) {
			alreadyvoted := votedusers[n.Author.ID]
			if !alreadyvoted && strings.Contains(n.Body, cmdLgtm) {
				m := members[n.Author.ID]
				if m == nil {
					// comment from a non member --> ignore
					continue
				}
				switch m.AccessLevel {
				case gl.OwnerPermission:
					lgtm = lgtm + nh.project.Weight.Owner
					if nh.project.Weight.Owner > 0 {
						lgtms = append(lgtms, fmt.Sprintf("*%s* is Owner, counts %d", m.Name, nh.project.Weight.Owner))
					}
				case gl.MasterPermissions:
					lgtm = lgtm + nh.project.Weight.Master
					if nh.project.Weight.Master > 0 {
						lgtms = append(lgtms, fmt.Sprintf("*%s* is Master, counts %d", m.Name, nh.project.Weight.Master))
					}
				case gl.DeveloperPermissions:
					lgtm = lgtm + nh.project.Weight.Developer
					if nh.project.Weight.Developer > 0 {
						lgtms = append(lgtms, fmt.Sprintf("*%s* is Developer, counts %d", m.Name, nh.project.Weight.Developer))
					}
				case gl.ReporterPermissions:
					lgtm = lgtm + nh.project.Weight.Reporter
					if nh.project.Weight.Reporter > 0 {
						lgtms = append(lgtms, fmt.Sprintf("*%s* is Reporter, counts %d", m.Name, nh.project.Weight.Reporter))
					}
				case gl.GuestPermissions:
					lgtm = lgtm + nh.project.Weight.Guest
					if nh.project.Weight.Guest > 0 {
						lgtms = append(lgtms, fmt.Sprintf("*%s* is Guest, counts %d", m.Name, nh.project.Weight.Guest))
					}
				}
				votedusers[n.Author.ID] = true
			}
		}
	}
	log15.Info("lgtm count information", "lgtm", lgtm, "lgtms", lgtms, "config", nh.project)
	return lgtm, lgtms
}

func (nh *mrNoteHandler) NotesUpdated(members gitlab.ProjectMemberMap, botid int, note string) error {
	notes, err := nh.cl.GetMRNotes(nh.project.Id, nh.project.MergeRequest.Id)
	if err != nil {
		return err
	}
	myaccess := members[botid]
	if myaccess == nil {
		log15.Error("crazy, but bot is not member of project")
		return fmt.Errorf("bot is not member of project")
	}

	lgtm, lgtms := nh.countLGTMs(members, notes)
	msg := ""
	switch getCommand(note) {
	case cmdRating:
		msg = fmt.Sprintf("ignoring comments before last commit `%s`, current rating %d/%d\n\n", nh.project.LastCommit.Format(time.RFC3339), lgtm, nh.project.MergeRequest.RequiredWeight)

		for _, l := range lgtms {
			msg = msg + fmt.Sprintf("  * %s\n", l)
		}
	case cmdLgtm:
		if len(lgtms) > 0 {
			msg = fmt.Sprintf("Ignoring comments before last commit `%s`\n\n", nh.project.LastCommit.Format(time.RFC3339))

			for _, l := range lgtms {
				msg = msg + fmt.Sprintf("  * %s\n", l)
			}

			if lgtm >= nh.project.MergeRequest.RequiredWeight {
				if nh.project.MergeRequest.MergeOnAgreement {
					msg = fmt.Sprintf("accept MR because LGTM's accumulate to %d", lgtm)
					if err = nh.cl.AcceptMR(
						nh.project.Id,
						nh.project.MergeRequest.Id,
						msg,
						nh.project.MergeRequest.RemoveSourceBranch,
						!nh.project.MergeRequest.WhenPipelineFails); err != nil {
						if gitlab.IsAuthorizationError(err) {
							msg = fmt.Sprintf("tried to accept MR because LGTM's accumulate to %d, but i'm not allowed to do so", lgtm)
						} else {
							log15.Error("accept mr", "error", err)
							msg = ""
							// if another error occured we do not know what happend. perhaps the MR was accepted or not
							// do do nothing, the error is logged and not more
						}
					}
				} else {
					msg = msg + fmt.Sprintf("\n\nMR can be accepted because LGTM's accumulate to %d, but i'm not told to do so", lgtm)
				}
			}
		}
	}
	if msg != "" {
		err = nh.cl.CreateMRNote(nh.project.Id, nh.project.MergeRequest.Id, msg)
	}

	return err
}

func getCommand(note string) string {
	if strings.Contains(note, cmdRating) {
		return cmdRating
	}
	return cmdLgtm
}
