package remote

import (
	"crypto/tls"
	"net/http"
	"reflect"
	"testing"
)

func TestRemote_client(t *testing.T) {
	type fields struct {
		Insecure bool
	}

	tests := []struct {
		name   string
		fields fields
		want   *http.Transport
	}{
		{name: "Insecure", fields: fields{Insecure: true}, want: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}},
		{name: "Secure", fields: fields{Insecure: false}, want: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: false},
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Remote{
				insecure: tt.fields.Insecure,
			}
			if got := r.Client().Transport; !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Remote.client() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInsecure(t *testing.T) {
	tests := []struct {
		name string
		opt  bool
		want bool
	}{
		{"Insecure", true, true},
		{"Secure", false, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var r Remote
			opt := Insecure(tt.opt)
			opt(&r)
			if r.insecure != tt.want {
				t.Errorf("Remote.Insecure = %v, want %v", r.insecure, tt.want)
			}
		})
	}
}
