package remote

import (
	"crypto/tls"
	"net/http"
)

// Remote holds the configuration options for remote access.
type Remote struct {
	insecure bool
}

func (r *Remote) Client() *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: r.insecure},
	}
	return &http.Client{Transport: tr}
}

type RemoteOpt func(*Remote)

func Insecure(b bool) RemoteOpt {
	return func(r *Remote) {
		r.insecure = b
	}
}

func New(opts ...RemoteOpt) *Remote {
	var c Remote
	for _, o := range opts {
		o(&c)
	}
	return &c

}
