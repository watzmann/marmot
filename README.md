# marmot

~~~
   _____                               __
  /     \ _____ _______  _____   _____/  |_
 /  \ /  \\__  \\_  __ \/     \ /  _ \   __\
/    Y    \/ __ \|  | \/  Y Y  (  <_> )  |
\____|__  (____  /__|  |__|_|  /\____/|__|
        \/     \/            \/
~~~

Marmot is a little bot who helps you when working with Gitlab. It counts
the number of `/lgtm`'s of a MR and posts a note about the current rating.
If desired, `marmot` also accepts the MR.

More info at [our project page](https://watzmann.io/projects/marmot/)