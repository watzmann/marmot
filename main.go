package main

import (
	"net/http"
	"os"

	"github.com/inconshreveable/log15"
	"gitlab.com/watzmann/marmot/bot"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app           = kingpin.New("marmot", "A command helper bot for gitlab projects")
	serve         = app.Command("serve", "Starts a mermat at a given address")
	listenAddress = serve.Arg("listen", "Listenaddress.").Default("127.0.0.1:8000").String()
	secretToken   = serve.Flag("secrettoken", "The secret token which must be provided by the caller").String()
	revision      string
	builddate     string
)

func main() {
	log15.Root().SetHandler(log15.CallerFileHandler(log15.StreamHandler(os.Stderr, log15.JsonFormat())))

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	// starts the mermat listener
	case serve.FullCommand():
		myself := os.Getenv("MARMOT_PRIVATE_TOKEN")
		if myself == "" {
			log15.Error("MARMOT_PRIVATE_TOKEN is empty")
			return
		}
		gitlaburl := os.Getenv("MARMOT_GITLAB_API_URL")
		if gitlaburl == "" {
			log15.Error("MARMOT_GITLAB_API_URL is empty")
			return
		}
		m := bot.New(
			revision, builddate,
			bot.WithGitlabURL(gitlaburl),
			bot.WithPrivateToken(myself),
			bot.WithSecretToken(*secretToken))
		srv := &http.Server{
			Addr:    *listenAddress,
			Handler: m,
		}
		log15.Info("serve", "revision", revision, "builddate", builddate)
		err := srv.ListenAndServe()
		if err != nil {
			log15.Error("cannot start marmot", "error", err)
		}
	}
}
