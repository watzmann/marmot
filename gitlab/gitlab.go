package gitlab

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/watzmann/marmot/remote"

	gl "github.com/xanzy/go-gitlab"
)

const (
	EventMergeRequest = "Merge Request Hook"
	EventNote         = "Note Hook"

	CanBeMerged = "can_be_merged"
)

var (
	errNotAllowed   = fmt.Errorf("Not Allowed")
	errUnAuthorized = fmt.Errorf("Unauthorized")
)

type ProjectMemberMap map[int]*gl.ProjectMember

func ParseMR(data []byte) (*gl.MergeEvent, error) {
	var res gl.MergeEvent
	return &res, json.Unmarshal(data, &res)
}

func ParseMRNote(data []byte) (*gl.MergeCommentEvent, error) {
	var res gl.MergeCommentEvent
	return &res, json.Unmarshal(data, &res)
}

func IsNotAllowed(e error) bool {
	return e == errNotAllowed
}

func IsUnauthorized(e error) bool {
	return e == errUnAuthorized
}

func IsAuthorizationError(e error) bool {
	return IsNotAllowed(e) || IsUnauthorized(e)
}

type Client struct {
	cl                *gl.Client
	me                *gl.User
	GetMRNotes        func(int, int) ([]*gl.Note, error)
	GetMergeRequest   func(int, int) (*gl.MergeRequest, error)
	GetFile           func(int, string, string) (string, error)
	GetProjectMembers func(pid int) (ProjectMemberMap, error)
	GetMe             func() (*gl.User, error)
	CreateMRNote      func(pid int, mid int, msg string) error
	AcceptMR          func(pid, mid int, msg string, removesource, pipelinesucc bool) error
}

func New(u, token string, insecureSkipVerify bool) (*Client, error) {
	remoteUrl, err := url.Parse(u)
	if err != nil {
		return nil, fmt.Errorf("cannot parse URL %q when creating gitlab client: %v", u, err)
	}
	service := remoteUrl.Scheme + "://" + remoteUrl.Host + "/api/v4"
	cl := gl.NewClient(remote.New(remote.Insecure(insecureSkipVerify)).Client(), token)
	cl.SetBaseURL(service)

	res := &Client{cl: cl}
	res.GetMRNotes = res.getMRNotes
	res.GetMergeRequest = res.getMR
	res.GetFile = res.getFile
	res.GetProjectMembers = res.getProjectMembers
	res.GetMe = res.getMe
	res.CreateMRNote = res.createMRNote
	res.AcceptMR = res.acceptMR
	return res, nil
}

func (c *Client) getMR(pid, iid int) (*gl.MergeRequest, error) {
	mr, _, err := c.cl.MergeRequests.GetMergeRequest(pid, iid)
	if err != nil {
		return nil, fmt.Errorf("cannot fetch MR: %v", err)
	}
	return mr, nil
}
func (c *Client) getMRNotes(pid, iid int) ([]*gl.Note, error) {
	notes, _, err := c.cl.Notes.ListMergeRequestNotes(pid, iid)
	if err != nil {
		return nil, fmt.Errorf("cannot fetch notes for MR: %v", err)
	}
	return notes, nil
}

func (c *Client) getFile(pid int, fn, ref string) (string, error) {
	opts := gl.GetFileOptions{
		Ref: &ref,
	}
	f, _, err := c.cl.RepositoryFiles.GetFile(pid, fn, &opts)
	if err != nil {
		return "", fmt.Errorf("cannot load file %q in ref %q from repo: %v", fn, ref, err)
	}
	b, err := base64.StdEncoding.DecodeString(f.Content)
	return string(b), err
}

func (c *Client) getMe() (*gl.User, error) {
	if c.me != nil {
		return c.me, nil
	}
	u, _, err := c.cl.Users.CurrentUser()
	if err != nil {
		return nil, fmt.Errorf("cannot query user: %v", err)
	}
	c.me = u
	return u, nil
}

func (c *Client) getProjectMembers(pid int) (ProjectMemberMap, error) {
	res := make(ProjectMemberMap)
	opts := gl.ListProjectMembersOptions{
		ListOptions: gl.ListOptions{
			Page:    0,
			PerPage: 100,
		},
	}
nextbatch:
	members, rsp, err := c.cl.ProjectMembers.ListProjectMembers(pid, &opts)
	if err != nil {
		return nil, fmt.Errorf("cannot fetch project members: %v", err)
	}
	for _, m := range members {
		res[m.ID] = m
	}
	if rsp.NextPage > 0 {
		opts.Page = rsp.NextPage
		goto nextbatch
	}
	return res, nil
}

func (c *Client) createMRNote(pid int, mid int, msg string) error {
	opt := gl.CreateMergeRequestNoteOptions{
		Body: &msg,
	}
	_, _, err := c.cl.Notes.CreateMergeRequestNote(pid, mid, &opt)
	if err != nil {
		return fmt.Errorf("cannot create MR note: %v", err)
	}
	return nil
}

func (c *Client) acceptMR(pid, mid int, msg string, removesource, pipelinesucc bool) error {
	opt := gl.AcceptMergeRequestOptions{
		MergeCommitMessage:        &msg,
		ShouldRemoveSourceBranch:  &removesource,
		MergeWhenPipelineSucceeds: &pipelinesucc,
	}

	_, rsp, err := c.cl.MergeRequests.AcceptMergeRequest(pid, mid, &opt)
	if err != nil {
		if rsp.StatusCode == http.StatusMethodNotAllowed {
			return errNotAllowed
		}
		if rsp.StatusCode == http.StatusUnauthorized {
			return errUnAuthorized
		}
		return fmt.Errorf("cannot merge project: %v", err)
	}
	return nil
}
