SHA := $(shell git rev-parse --short=8 HEAD)
GITVERSION := $(shell git describe --long)
VERSION := $(shell echo $(GITVERSION) | cut -f1 -d-)
SERIAL := $(shell echo $(GITVERSION) | cut -f2 -d-)-$(SHA)
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
BUILDDATE := $(shell date --rfc-3339=seconds)

.PHONY: test build up image push deploy

build:
	mkdir -p bin
	CGO_ENABLED=0 go build -ldflags "-X 'main.revision=$(VERSION).$(SERIAL)' -X 'main.builddate=$(BUILDDATE)'" -o bin/marmot

test:
	go test -v -race -cover $(shell go list ./...)

up:
	MARMOT_GITLAB_API_URL=https://gitlab.com go run main.go serve

image:
	docker build -t $(CI_REGISTRY_IMAGE):v$(SHA) -t $(CI_REGISTRY_IMAGE):latest .

push:
	docker push  $(CI_REGISTRY_IMAGE):v$(SHA)
	docker push $(CI_REGISTRY_IMAGE):latest
