module gitlab.com/watzmann/marmot

go 1.14

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf
	github.com/go-stack/stack v1.7.0
	github.com/google/go-querystring v0.0.0-20170111101155-53e6ce116135
	github.com/inconshreveable/log15 v0.0.0-20171019012758-0decfc6c20d9
	github.com/mattn/go-colorable v0.0.9
	github.com/mattn/go-isatty v0.0.3
	github.com/xanzy/go-gitlab v0.7.3
	golang.org/x/sys v0.0.0-20171207184313-a0f4589a76f1
	gopkg.in/alecthomas/kingpin.v2 v2.2.5
	gopkg.in/yaml.v2 v2.0.0-20171116090243-287cf08546ab
)
